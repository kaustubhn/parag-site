var express = require('express');
var app = express();

app.use(express.static('public'));

// app.all('*', function(req, res) {
//   res.redirect("http://localhost:8081");
// });


app.get('/', function (req, res) {
   res.sendFile( __dirname + "/" + "index.html" );
})

app.use("/css", express.static(__dirname + '/css'));

app.use("/js", express.static(__dirname + '/js'));
app.use("/fonts", express.static(__dirname + '/fonts'));
app.use("/images", express.static(__dirname + '/images'));

var server = app.listen(8081, function () {

  var host = server.address().address
  var port = server.address().port

  console.log("Example app listening at http://%s:%s", host, port)

})
